PyConZA Roles Documentation
===========================

This describes the various roles and timelines for running
[PyConZA](https://za.pycon.org).

The rendered version of the documentation can be found on
[readthedocs](http://pyconza-role-descriptions.readthedocs.io/).

