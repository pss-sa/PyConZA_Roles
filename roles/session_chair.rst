Session Chairs
==============

* Attend the session chair briefing at the start of the conference.
* Ensure you have been added to the Session Chair WhatsApp group
* Try get to your venue about 10 minutes before your slot to introduce yourself to speakers (who may want to test their equipment in the room) and check that all is well.

  - If required for the venue, ensure there are volunteers to run the microphone around the room during the question session.

* Introduce each speaker.
* Give your speakers time notifications -- usually for 10 minutes to go and 5 minutes to go.
* Open the floor for questions at the end of each talk.

  - Point out questions to the mic runners when required, since they are often poorly placed to observe the whole room.

* Close questions with enough time for the next talk so set up and start.
* Make any announcements that need to be made.
  
  - Watch the Whatsapp group for anything that comes up during a session

* Respond to any unusual circumstances (sound problems, for example.)
  
  - Contact appropriate people (usually video team) if problems can't be resolved quickly.
  - Inform other session chairs and organisers of any significant delays via the WhatsApp group when they happen
