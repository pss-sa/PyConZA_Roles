=====
Talks
=====


Talks Coordinator
=================

Description
-----------

The first point of contact for speakers submitting talks to the conference.

Responsiblities
---------------

* Send out the call for submissions to the various places

  - python-conferences and the local python mailing lists
  - Whichever local meetup groups seem relevant

* Send out "deadline extended" notifications if required.

* Reply to each talk submission in good time

  - 24 hour turn-around is a good target to aim for
  - Mark talks as "Under Consideration" once they've been responded to on the site.

* Manage the talk submission process

  - Ask for clarifications / improvements of the initial submission
  - Ask for information not included in the submission if it seems appropriate (early decisions, travel funding, visa requirements, etc).
  - Review and re-ask as needed
  - Followup with speakers for questions that haven't been answered

* Pass talks on the review stage when they're ready

  - Create a slack thread for each talk

    + Title, authors & url need to be specified
  - Post initial comments and opinions
  - Follow and engage in any discussion with reviewers as appropriate
  - Note decision on the slack thread once it's made.

* Inform submitter of the decision (Accepted, Declined)

  - Explain the "Provisionally Accepted status"
  - Inform Accepted submitters of relevant extra functions (speakers dinner, photograph, etc).
  - Ask about scheduling constraints.

* Inform speakers once their talks have been scheduled

Talk Reviewer
=============

* Read talk abstracts once they've been submitted for review
* Provide feedback on abstract contents
* Give a Accept / Decline opinion for each talk reviewed.


Talk Schedule
=============

* Draw up dummy schedule as soon as possible

  - Use placeholder pages for talks & other events
  - Important to capture the intended structure of the conference, and update it as things evolve.

* Once enough talks have been accepted, start adding them to the schedule.

  - Look for common themes that would help group related talks.
  - Guess at talk popularity for different venue sizes
  - Bear in mind any scheduling constraints that apply.

* Update schedule based on feedback from talk submitters and the talk coordinator.

* Manage on the fly updates during the conference. Common changes include

  - Emergency talk deployed.
  - Adjusting slot times due to technical issues.


Lightning Talks
===============

* Respond to lightning talk submissions as they come in.

  - Generally accept submissions, unless the schedule is full or there's some obvious reason to reject (such as duplicate topics)

* Add accepted lightning talks to the web site

* Let accepted speakers know when their talk has been added
