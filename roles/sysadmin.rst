=====================
System Administration
=====================

Website
=======

The PyCon ZA website is hosted on the `CTPUG <https://www.ctpug.org.za>`_ server,
with the wildcard domain entry provided by the FSF.

The website is powered by a `wafer <https://github.com/CTPUG/wafer>`_ instance.

The sysadmin team is responsible for maintaining the infrastructure needed for
the website, which includes updating and maintaining the wafer installation
and deploying any site updates that require access to the system.

Organiser Repo
==============

For sharing information amongst the committee, we host per-year private git
repos on the `CTPUG <https://www.ctpug.org.za>`_ server.

This repo is administered using
`gitolite <http://gitolite.com/gitolite/index.html>`_.
The sysadmin team is responsible for setting up the repo, managing the access
keys to the repository and managing any notification settings (irker, slack, etc)
required.

PyCon ZA theme repo
===================

We create and run a repo for the theming and additional features needed for
PyCon ZA. This is created under the CTPUG organisation on
`github <https://github.com/CTPUG/>`_.

Email aliases
=============

We maintain a number of email aliases for the conference.

The core email aliases are:

* **team@za.pycon.org** - This is the general catch-all contact address. The core
  organising committee should recieve email sent to this address.
* **talks@za.pycon.org** - The talk contact address. The core talk review committee
  should recieve mail sent to this address.
* **funding@za.pycon.org** - Contact address for funding / bursary applications.
  The funding committee should be on this aliases. For historical reasons,
  the **bursaries@za.pycon.org** alias forwards to this address as well.
* **services@za.pycon.org** - Email for subscription / service management. The
  sysadmin team should recieve email to this address.
* **conduct@za.pycon.org** - Code of conduct contact address. The members of the
  code of conduct committee should be on this alias during and after the
  conference.

Other email aliases can be created as needed, for specific side events or so
forth.
