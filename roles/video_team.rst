Video Team
==========

The video team mostly uses the same structure and roles as the DebConf video
team. We rely on volunteers to fulfil the various `roles`_ during talks. They
have also `documented`_ the layout and hardware they use and that PyConZA bases
our set-up on. The main difference is that we hire all the equipment we need and
only use one camera per talk room.

Instead of the Talk Meister used at DebConfs, PyConZA uses a `Session Chair`_.
This role has the same responsibilities of a DebConf Talk Meister, as well as
some extra responsibilities.

.. _roles: https://debconf-video-team.pages.debian.net/docs/volunteer_roles.html
.. _documented: https://debconf-video-team.pages.debian.net/docs/index.html
.. _Session Chair: session_chair.html
