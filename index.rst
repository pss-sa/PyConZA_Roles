.. PyConZA Role Descriptions documentation master file, created by
   sphinx-quickstart on Thu Nov  2 17:06:16 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyConZA Role Descriptions
=========================

Contents:

.. toctree::
   :maxdepth: 2

   roles/chair
   roles/secretary
   roles/event_cordinator
   roles/sponsors
   roles/venue
   roles/talks
   roles/funding_committee
   roles/budget
   roles/design
   roles/swag
   roles/session_chair
   roles/video_team
   roles/sysadmin


PyConZA Timeline
================

Contents:

.. toctree::
   :maxdepth: 2

   timeline/timeline.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

About this document
===================

The documentation is managed by the `PSS-SA <https://www.pss-sa.org>`_
for teams looking to organise PyCon ZA. To contribute more information
or submit corrections, please create a pull request against the
`gitlab <https://gitlab.com/pss-sa/PyConZA_Roles>`_ project.

