================
PyConZA Timeline
================


Well before conference date
===========================

* Update za.pycon.org to mention intended year
* Form conference oganising committe and assign roles

After committee is established
==============================

* Source a venue, and provisionally book it
* Start nagging sponsors
* Setup a new wafer instance on za.pycon.org
* Sort out basic theming and design for the site for the year

Once venue is secured
=====================

* Open talk submissions
* Announce talk submission deadline of 1 month before the conference date
* Announce tutorial submission deadline of 2 months before the conference date
* Decide on a travel funding submission deadline and announce it
* Announce talk and tutorial submissions everywhere (python-conferences, local PUG lists, various meetups, etc.)
* Add za.pycon.org to appropriate calendars (LWN, python-events, OfferZen, etc.)
* Decide on ticket prices and types.
* Decide on date for ticket sales to open.
* Decide on early bird ticket criteria (dates, numbers, etc.)
* Draw up draft schedule, so everyone is clear on rooms and times involved.
* Announce conference on various channels.

Ongoing once talk and tutorial submissions are open
===================================================

* Reponse to new submissions (talk chair).
* Try source tutorials that match requests from previous years.
* Decide on any early talk decision requests.
* Start collecting travel sponsorship requests.

Conference date -2 months
=========================

* Close tutorial submissions.
* Decide on which tutorials to accept.
* Set tutorial prices with submitters and venue chair.
* Open ticket sales for tutorials.
* Announce tutorial ticket sales everywhere.

Conference date -1 month
========================

* Extend talk deadline if required.
* Decide on talk submissions already recieved.
* Notify speakers of provisionally accepted talks.
* Start work on filling in schedule as speakers register.

Conference data -2 weeks
========================

* Announce schedule details to registered attendees
